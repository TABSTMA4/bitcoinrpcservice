package bitcoinrpcservice

// Bitcoinrpcservice holds some properties for the rpcclient
type Bitcoinrpcservice struct {
	port     string
	user     string
	password string
}

// RequestBody is used to call methods from bitcoin
type requestBody struct {
	Jsonrpc string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
}

type getNewAddressResponse struct {
	Address string `json:"result"`
}

type getAddressInfoResponse struct {
	getAddressInfoResult `json:"result"`
}

type getAddressInfoResult struct {
	PublicKey    string `json:"pubkey"`
	ScriptPubKey string `json:"scriptPubKey"`
}

type createMultisigResponse struct {
	Script string `json:"redeemScript"`
}
