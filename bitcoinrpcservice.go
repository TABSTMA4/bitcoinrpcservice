package bitcoinrpcservice

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// New creates a new bitcoinrpcservice-object
func New(port string, username string, password string) Bitcoinrpcservice {
	service := Bitcoinrpcservice{port, username, password}
	return service
}

// Help format the request body correctly
func getRequestBody(method string, params []interface{}) []byte {
	bodyStruct := requestBody{"1.0", method, params}
	body, _ := json.Marshal(bodyStruct)

	return body
}

// MakeRequest requests bitcoinrpc methods
func (service Bitcoinrpcservice) MakeRequest(method string, params []interface{}) ([]byte, error) {
	client := &http.Client{}
	requestBody := getRequestBody(method, params)

	req, _ := http.NewRequest("POST", "http://127.0.0.1:"+service.port, bytes.NewBuffer(requestBody))
	req.SetBasicAuth(service.user, service.password)
	res, err := client.Do(req)

	if err != nil {
		return nil, err
	}

	bodyBytes, _ := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	return bodyBytes, nil
}

// GetNewAddress returns a new Bitcoin Address
func (service Bitcoinrpcservice) GetNewAddress() string {
	response, err := service.MakeRequest("getnewaddress", []interface{}{})

	if err != nil {
		fmt.Println("failed getting a new address")
		return ""
	}

	var addressObject getNewAddressResponse
	json.Unmarshal(response, &addressObject)

	return addressObject.Address
}

// GetNewPublicKey returns a newly generated public key
func (service Bitcoinrpcservice) GetNewPublicKey() string {
	address := service.GetNewAddress()

	response, err := service.MakeRequest("getaddressinfo", []interface{}{address})

	if err != nil {
		fmt.Println("failed getting a new public key")
		return ""
	}

	var getAddressInfoResponse getAddressInfoResponse
	err = json.Unmarshal(response, &getAddressInfoResponse)

	return getAddressInfoResponse.PublicKey
}

// CreateMultisig creates a signature-script for dynamic numbers of keys
func (service Bitcoinrpcservice) CreateMultisig(requiredSignatures int, publicKeys []string) string {
	fmt.Println(publicKeys)

	response, err := service.MakeRequest("createmultisig", []interface{}{requiredSignatures, publicKeys})

	if err != nil {
		fmt.Println("Error creating multisig. (is bitcoind running?)")
		return ""
	}

	fmt.Println(response)
	var createMultisigResponse createMultisigResponse
	err = json.Unmarshal(response, &createMultisigResponse)

	return createMultisigResponse.Script
}
